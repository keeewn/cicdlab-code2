import pytest
from hello.hello import app

@pytest.fixture
def client():
    return app.test_client()

def test_hello_world(client):
    response = client.get('/')
    assert response.data == b'Hello, World!'

# def test_hello_name(client):
#     response = client.get('/bobby')
#     assert response.data == b'Hello, bobby!'
